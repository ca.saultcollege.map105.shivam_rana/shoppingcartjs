/* here are the arrays used for project 1 */

var filenames = ["106020.jpg", "116010.jpg", "120010.jpg"];
/*var filenames = new Array();

filenames[0] = new Image();
filenames[0].src = "../images/106020.jpg";

filenames[1] = new Image();
filenames[1].src = "../images/116010.jpg";

filenames[2] = new Image();
filenames[2].src = "../images/116010.jpg";*/



var titles = ["Girl with a Pearl Earring", "Artist Holding a Thistle", "Portrait of Eleanor of Toledo"];
var quantities = [3, 1, 20];
var prices = [80, 125, 75];

/* 
   NOTE: parallel arrays are not an ideal way to represent this data.
         We have done this to simplify this exercise.
         
         A better approach would be to turn these parallel arrays
         into an array of cart item objects. Objects are used in
         projects 2 and 3.
*/         